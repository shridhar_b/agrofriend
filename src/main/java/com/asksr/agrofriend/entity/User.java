package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class User {
    @Id
    private int id;
    private String firstName;
    private String lastName;
    private Date dob;
    private int age;
    private String mobileNumber;
    private Date createdTime;
    private Date updatedTime;
    private List<Roles> roles;
    private List<Address> addresses;
    private List<Product> products;
}
