package com.asksr.agrofriend.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
@Getter
@Setter
@Entity
public class OrderDetail {
    @Id
    private int id;
    private String productName;
    private String description;
    private double quantity;
    private double prize;
    private int orderId;
    private Date createdTime;
    private Date updatedTime;

}
