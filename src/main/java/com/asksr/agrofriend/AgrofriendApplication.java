package com.asksr.agrofriend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgrofriendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgrofriendApplication.class, args);
	}

}
